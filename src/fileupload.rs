use std::fmt::Display;

pub trait Uploadable: Display {
    fn verify(&self, bytes: &[u8]) -> bool {
        let data: String = self.bytes_to_string(&bytes);
        for magic in self.get_magic(){
            if data.starts_with(&magic){
                return true;
            }
        }
        return false;
    }

    fn bytes_to_string(&self, bytes: &[u8]) -> String {
        let strings: Vec<String> = bytes.iter()
            .map(|b| format!("{:02X}", b))
            .collect();
        strings.join(" ").to_ascii_uppercase()

    }
    fn get_magic(&self) -> Vec<String>;
    fn get_extension(&self) -> &'static str;
}

pub struct PNG;
pub struct JPEG;
pub struct DOCX;

impl Display for PNG{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "PNG")
    }
}
impl Uploadable for PNG {
    fn get_magic(&self) -> Vec<String> {
        return vec![String::from("89 50 4E 47 0D 0A 1A 0A"), String::from("50 89 47 4E 0A 0D 0A 1A")]
    }

    fn get_extension(&self) -> &'static str {
        ".png"
    }
}

impl Display for JPEG {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "JPEG")
    }
}

impl Uploadable for JPEG{
    fn get_magic(&self) -> Vec<String> {
        vec![String::from("FF D8 FF E0")]
    }

    fn get_extension(&self) -> &'static str {
        ".jpg"
    }
}

impl Display for DOCX{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "DOCX")
    }
}

impl Uploadable for DOCX {
    fn get_magic(&self) -> Vec<String> {
        vec![String::from("50 4B 03 04")]
    }

    fn get_extension(&self) -> &'static str {
        ".docx"
    }
}