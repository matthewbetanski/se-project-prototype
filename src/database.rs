use async_once::AsyncOnce;
use bcrypt::{hash, DEFAULT_COST, hash_with_salt};
use rand::{distributions::Alphanumeric, Rng};
use sqlx::{PgPool, postgres::PgPoolOptions, QueryBuilder, Postgres, query_builder};

use crate::Credentials;

lazy_static::lazy_static! {
    static ref POOL: AsyncOnce<PgPool> = AsyncOnce::new(async {
        dotenv::dotenv().expect("Unable to load environment variables from .env file");

        let db_url = std::env::var("DATABASE_URL").expect("Unable to read DATABASE_URL environment variable");

        PgPoolOptions::new()
            .max_connections(100)
            .connect(&db_url)
            .await.expect("Unable to connect to Postgres")
    });
}


pub async fn create_user(credentials: &Credentials) -> Result<(), sqlx::Error>{
    let mut query_builer: QueryBuilder<Postgres> = QueryBuilder::new("INSERT INTO users (username, password) VALUES (");
    let mut separated = query_builer.separated(",");
    separated.push_bind(credentials.username.clone())
        .push_bind(credentials.password.clone());
    query_builer.push(");");
    let query = query_builer.sql();
    println!("{query}");
    if let Err(error) = query_builer.build().execute(POOL.get().await).await {
        println!("{error}");
        Err(error)
    } else { 
        Ok(())
    }
}

pub async fn login_user(credentials: &Credentials) -> Result<bool, sqlx::Error> {
    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new("SELECT password FROM users WHERE username = ");
    query_builder.push_bind(&credentials.username);

    if let Some(db_password) = query_builder.build_query_scalar::<String>().fetch_optional(POOL.get().await).await? {
        if db_password.eq(&credentials.password){
            return Ok(true)
        }
    }

    Ok(false)
}

pub async fn upload_file_database(file_path: &String, username: &String) -> Result<(), sqlx::Error> {
    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new("INSERT INTO upload (username, filepath) VALUES (");
    let mut seperated = query_builder.separated(",");
    seperated.push_bind(&username)
        .push_bind(&file_path);

    query_builder.push(")");
    if let Err(error) = query_builder.build().execute(POOL.get().await).await {
        println!("{error}");
        return Err(error);
    }
    Ok(())
}