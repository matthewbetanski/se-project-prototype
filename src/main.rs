use actix_identity::{IdentityMiddleware, Identity};
use actix_session::Session;
use actix_session::config::PersistentSession;
use actix_session::{config::BrowserSession, storage::RedisActorSessionStore, SessionMiddleware};
use actix_web::cookie::time::Duration;
use actix_web::{get, App, HttpResponse, HttpServer, Responder, cookie::Key};
use actix_cors::Cors;
use rand::Rng;
use rand::distributions::Alphanumeric;

mod database;
mod fileupload;
mod routes;
pub use routes::*;
pub use fileupload::*;
pub use database::*;
mod models;
pub use models::*;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .wrap(Cors::permissive())
            .wrap(IdentityMiddleware::default())
            .wrap(session_middleware())
            .service(home)
            .service(login)
            .service(logout)
            .service(logout_page)
            .service(login_page)
            .service(create_account)
            .service(create_page)
            .service(upload)
            .service(upload_page)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await?;

    Ok(())
}


#[get("/")]
async fn home(identity: Option<Identity>, session: Session) -> impl Responder {
    if let Some(user) = identity {
        session.renew();
        HttpResponse::Ok().body(format!("Hello: {}", user.id().unwrap()))
    } else {
        HttpResponse::Ok().body("Hello World!")
    }
}

fn session_middleware() -> SessionMiddleware<RedisActorSessionStore>{
    SessionMiddleware::builder(
        RedisActorSessionStore::new("127.0.0.1:6379"), Key::generate()
    ).cookie_name(String::from("testing-cookie"))
    .session_lifecycle(PersistentSession::default().session_ttl(Duration::hours(2)))
    .cookie_http_only(true)
    .cookie_same_site(actix_web::cookie::SameSite::Strict)
    .cookie_secure(false)
    .build()
}

fn generate_secret_key() -> Key{
    let string: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(512)
        .map(char::from)
        .collect();
    Key::from(string.as_bytes())
}

