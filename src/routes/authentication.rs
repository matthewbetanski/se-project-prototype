use actix_identity::Identity;
use actix_session::{Session, SessionStatus};
use actix_web::{Responder, HttpResponse, get, post, web::{Json, Form, Redirect}, HttpRequest, HttpMessage, http::StatusCode};
use serde::Deserialize;

use crate::{Credentials, create_user, login_user};

#[get("/create")]
pub async fn create_page(session: Session)->impl Responder{
        HttpResponse::Ok().body(r#"
        <title>Create Account!</title>
        <p>Create an Account!</p>
        <form action="/create" method="post">
            <input type="text" name="username">
            <input type="password" name="password">
            <button>Create Account</button>
        </form>  
        "#)
}

#[post("/create")]
pub async fn create_account(input: Form<Credentials>, session: Session) -> impl Responder{
    match create_user(&input).await {
        Ok(_) => {
            if let Err(_) = session.insert("user", &input.username) {
                HttpResponse::InternalServerError().body("Error Creating Account")
            } else {
                HttpResponse::Ok().body("Account Created!")
            }
        }
        Err(error) => {
            HttpResponse::BadRequest().body("Error Creating Account!")
        }
    }
}

#[get("/login")]
pub async fn login_page(session: Session) -> impl Responder{
    HttpResponse::Ok().body(r#"
    <title>Login!</title>
    <p>Login to Account!</p>
    <form action="/login" method="post">
        <input type="text" name="username">
        <input type="password" name="password">
        <button>Log In!</button>
    </form>
    "#)
}

#[post("/login")]
pub async fn login(input: Form<Credentials>, request: HttpRequest) -> impl Responder{
    match login_user(&input).await {
        Ok(logged_in) => {
            match logged_in {
                true => {
                        if let Err(error) = Identity::login(&request.extensions(), input.username.clone()){
                            println!("{error}");
                        }
                        HttpResponse::Found().append_header(("Location", "/")).finish()
                }
                false => {
                    HttpResponse::Unauthorized().body("The entered username or password was incorrect, try again")
                }
            }
        }
        Err(error) => {
            println!("{error}");
            HttpResponse::InternalServerError().body("Internal error occured during login process")
        }
    }
}

#[get("/logout")]
pub async fn logout_page(session: Session) -> impl Responder {
        HttpResponse::Ok().body(r#"
            <title>Logout!</title>
            <p>Logout of Account!</p>
            <form action="/logout" method="post">
                <input type="submit", name="Submit">
            </form>
        "#)
}
#[post("/logout")]
pub async fn logout(identity: Option<Identity>, session: Session) -> impl Responder {
        if let Some(user) = identity{
            user.logout();
            session.purge();
        }
        Redirect::to("/").using_status_code(StatusCode::FOUND)
}