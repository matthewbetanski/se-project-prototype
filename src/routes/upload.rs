use std::{io::{BufReader, Read, BufWriter, Write}, fs::File};

use actix_identity::Identity;
use actix_multipart::form::{MultipartForm, tempfile::TempFile};
use actix_session::Session;
use actix_web::{HttpResponse, Error, web::{post, self, Redirect}, post, Responder, get};
use rand::{distributions::Alphanumeric, Rng};
use serde::{Deserialize, Serialize};

use crate::{Uploadable, PNG, upload_file_database, JPEG, DOCX};

#[derive(MultipartForm)]
struct UploadForm{
    file: TempFile
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all="lowercase")]
enum UploadType{
    IMAGE,
    DOCUMENT
}

fn generate_file_types(file_types: UploadType) -> Vec<Box<dyn Uploadable>> {
    match file_types {
        UploadType::IMAGE => {
            vec![Box::new(PNG), Box::new(JPEG)]
        }
        UploadType::DOCUMENT => {
            vec![Box::new(DOCX)]
        }
    }
}

#[get("/upload")]
async fn upload_page() -> impl Responder {
    HttpResponse::Ok().body(r#"

        <form method="post" action="/upload/image" id="myform" name="myform" enctype="multipart/form-data">
            <input type="file" name="file"><br>
            <input id="image" type="radio" name="type" onclick="myform.action='/upload/image';" checked>
            <label for="image">Image<br></label>
            <input id="document" type="radio" name="type" onclick="myform.action='/upload/document';">
            <label for="document">Document</label><br>
            <button>Upload File</button>
        </form>
    "#)
}

#[post("/upload/{type}")]
async fn upload(payload: MultipartForm<UploadForm>, identity: Option<Identity>, path: web::Path<UploadType>, session: Session) -> impl Responder {
    let file_type = path.into_inner();
    let allowed = generate_file_types(file_type);
    if let Some(user) = identity {
        session.renew();
        let file = payload.file.file.as_file();
        
        let mut reader = BufReader::new(file);
        let mut bytes = Vec::new();
        if let Err(error) = reader.read_to_end(&mut bytes) {
            return HttpResponse::InternalServerError().body("Issue parsing file");
        }

        let mut valid = false;
        let mut file_name: String = rand::thread_rng()
                                    .sample_iter(&Alphanumeric)
                                    .take(20).map(char::from)
                                    .collect();

        for file_type in &allowed {
            if file_type.verify(&bytes) {
                file_name.push_str(file_type.get_extension());
                valid = true;
                break;
            }
        }

        if !valid {
            let mut valid_types = String::from("Invalid file type. Allowed file types are: ");
            for file_type in &allowed{
                valid_types.push_str(&format!("{}, ", &file_type.to_string()));
            }
            valid_types = valid_types.trim_end_matches(", ").to_string();
            return HttpResponse::Forbidden().body(valid_types);
        }
        
        let file_path = format!("upload/{}", file_name);
        let mut stored = match File::create(&file_path){
            Ok(file) => file,
            Err(error) => {
                println!("{error}");
                return HttpResponse::InternalServerError().body("Unable to save file");
            }
        };
        let mut writer = BufWriter::new(stored);
        if let Err(error) = writer.write_all(&bytes){
            std::fs::remove_file(&file_path);
            println!("Unable to write to file");
            return HttpResponse::InternalServerError().body("Unable to save file");
        }

        let username = user.id().unwrap();
        if let Err(error) = upload_file_database(&file_name, &username).await {
            std::fs::remove_file(&file_path);
            println!("Unable to save file info to DB");
            return HttpResponse::InternalServerError().body("Unable to save file");
        }
        HttpResponse::Ok().body("Uploaded")
    } else {
        println!("Attempting to redirect");
        HttpResponse::Found().append_header(("Location", "/login")).finish()
    }
}