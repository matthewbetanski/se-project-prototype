-- Add up migration script here
CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE upload (
    id BIGSERIAL PRIMARY KEY,
    username TEXT NOT NULL,
    filepath TEXT UNIQUE NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username)
)